## About the project

### View [Demo](https://clone-react-c8950.web.app/)

### Techniques:
- React (hooks), context-api, react-dom
- Firebase (setup, deploy)
- Material icons, UI

### Usage
1. Click sign in to login page
   - Create an account
2. Navigate to Home page
   - Add item(s) into shopping cart
3. Click on shopping cart (icon) navigate to checkout page
   - Remove item(s) from shopping cart
