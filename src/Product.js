import React from 'react';
import './Product.scss';
import { useStateValue } from './StateProvider';

function Product({ id, title, price, rating, image }) {

  const [{}, dispatch] = useStateValue();

  const addToCart = () => {
    // dispatch the added item(s) into the data layer
    dispatch ({
      type: 'ADD_TO_CART',
      item: {
        id: id,
        title: title,
        price: price,
        rating: rating,
        image: image
      },
    });
  };

  return (
    <div className="product">
      <div className="product__info">
        <p className="product__title">{title}</p>
        <div className="product__price">
          <small>$</small>
          <strong>{price}</strong>
        </div>
        <div className="product__rating">
          {Array(rating).fill().map((_, i) => (
            <p className="product__ratingIcon">⭐</p>
          ))}
        </div>
      </div>
      <img
        className="product__img"
        src={image} 
        alt="" />
      <button
        className="product__btn"
        onClick={addToCart}>
        Add to Shopping Cart
      </button>
    </div>
  )
}

export default Product;
