import React from 'react';
import './Checkout.scss';
import CheckoutProduct from './CheckoutProduct';

// state
import Subtotal from './Subtotal';
import { useStateValue } from './StateProvider';

// tools
import { Flipper } from 'react-flip-toolkit';

function Checkout() {
  const [{ cartList, user }, dispatch] = useStateValue();

  return (
    <div className="checkout">
      <div className="checkout__left">
        <img
          className="checkout__ad"
          src="https://images-na.ssl-images-amazon.com/images/G/01/credit/img16/CCMP/newstorefront/YACC-desktop-nonprime-banner3.png"
          alt=""
          />
          <h3>Hello {user?.email}</h3>
          <h2 className="checkout__title">Your Shopping List</h2>
          <Flipper flipKey={cartList}>
            {cartList.map(item => (
              <CheckoutProduct
                key={`item${item.id}`}
                id={item.id}
                title={item.title}
                price={item.price}
                rating={item.rating}
                image={item.image}
              />
            ))}
          </Flipper>
      </div>
      <div className="checkout__right">
        <Subtotal />
      </div>
    </div>
  )
}

export default Checkout;
