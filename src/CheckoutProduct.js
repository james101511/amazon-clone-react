import React from 'react';
import './CheckoutProduct.scss';

// state
import { useStateValue } from './StateProvider';

// tools
import { Flipped, spring } from 'react-flip-toolkit';

function CheckoutProduct({id, title, price, rating, image}) {
  const [{}, dispatch] = useStateValue();

  const removeFromCart = () => {
    // dispatch the remove item(s) from the data layer
    dispatch({
      type: 'REMOVE_FROM_CART',
      id: id
    });
  };

  /**
    * flip animation
    * check: https://codesandbox.io/s/list-transitions-ju549?file=/src/Card.js
    */
  const flipId = `item-${id}`;
  const onElementAppear = (e, index) => {
      spring({
         onUpdate: val => {
            e.style.opacity = val;
         },
         delay: index * 50
      });
   }
  const onElementRemove = (e, index, removeElement) => {
    spring({
        config: { overshootClamping: true },
        onUpdate: (val) => {
          e.style.transform = `scaleY(${1 - val})`
        },
        delay: index * 50,
        onComplete: removeElement
    })

    return () => {
        e.style.opacity = "";
        removeElement();
    }
  } 
  
  return (
    <Flipped
      flipId={flipId}
      key={flipId}
      onAppear={onElementAppear}
      onExit={onElementRemove}>
      <div className="checkoutProduct">
        <img
          className="checkoutProduct__img"
          src={image}
          alt="" />
        <div className="checkoutProduct__info">
          <p className="checkoutProduct__title">{title}</p>
          <div className="checkoutProduct__price">
            <small>$</small>
            <strong>{price}</strong>
          </div>
          <div className="checkoutProduct__rating">
            {Array(rating).fill().map((_, i) => (
              <p className="checkoutProduct__ratingIcon">⭐</p>
            ))}
          </div>
          <button
            className="checkoutProduct__btn"
            onClick={removeFromCart}>
            Remove from Shopping Cart
          </button>
        </div>
      </div>
    </Flipped>
  )
}

export default CheckoutProduct
